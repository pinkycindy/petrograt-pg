<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Order;
use frontend\models\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\Item;
use frontend\models\OrderItem;
use frontend\models\Customer;
use yii\data\ActiveDataProvider;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $cookies = Yii::$app->request->cookies;
        $totalBeli = $cookies->get('totalBeli');
        $total = $totalBeli->value;
        $simpanItem = [];

        for($i=0;$i<=$total;$i++){
            $beli = $cookies->get('beli' . $i);
            $itemTotal = $beli->value;

            array_push($simpanItem, $itemTotal);

        }
        $order = Item::find()->where(['id'=> $simpanItem]);
        $dataProvider = new ActiveDataProvider(['query'=> $order,
            ]);
        return $this->render('index', ['dataProvider' => $dataProvider,]);


        /*$searchModel = new OrderSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    
    */
}


    public function actionBayar(){        
        $transaction= Order::getDb()->beginTransaction();
         try{
            $customer = Customer::find()->where(['user_id' => yii::$app->user->identity->id])->one();
            $order = new Order();
            $order->customer_id = $customer->id;
            $order->date = date('Y-m-d H:i:s');
            $order->save();
            $orderQuery = Order::find()->where(['customer_id' => $customer->id])->orderBy('id DESC')->one();
             $cookies = yii::$app->request->cookies;
             $totalBeli=$cookies->get('totalBeli');
             $total=$totalBeli->value;
             for($i=0; $i<=$total; $i++){
                 $beli=$cookies->get('beli'.$i);
                 $barangTotal = $beli->value;

                 $orderItem = new OrderItem();
                 $orderItem->order_id = $orderQuery->id;
                 $orderItem->item_id = $barangTotal;
                $orderItem->save();
             }
             $transaction->commit();
            //hapus cookie 
            $cookies = yii::$app->response->cookies;
            for($i=0;$i<=$total; $i++){
                $cookies->remove('beli' . $i);
            }
            $cookies->remove('totalBeli');
         }
         catch(\exception $e){
             $transaction->rollback();
             throw $e;
         }
         return $this->redirect(['item/index']);
    }
    /**

     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
