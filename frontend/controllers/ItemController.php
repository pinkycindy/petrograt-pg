<?php

namespace frontend\controllers;

use Yii;
use frontend\models\item;
use frontend\models\itemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Cookie;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use frontend\models\itemCategory;


/**
 * ItemController implements the CRUD actions for item model.
 */
class ItemController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['buy'],
                'rules' => [
                    [
                        'actions' => ['buy'],
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['buy'],
                'rules' => [
                    [
                        'actions' => ['buy'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all item models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query = item::find();
        $pages = new \yii\data\Pagination(['totalCount' => $query->count()]);
        $models = $query->offset($pages->offset)->limit($pages->limit)->all();
        $kategori = ItemCategory::find()->all();
       
        $dataProvider =new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 5,
                ],
            ]);

        return $this->render('index', [
            'models' => $models,
            'pages' => $pages,
            'kategori' => $kategori,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single item model.
     * @param integer $id
     * @return mixed
     */

    public function actionIndexFilter($id){
        $query = item::find()->where(['category_id' => $id]);
        $pages = new \yii\data\Pagination(['totalCount'=>$query->count()]);
        $models = $query->offset($pages->offset)->limit($pages->limit)->all();

        $kategori = ItemCategory::find()->all();
        $querykategori = Item::find()->where(['category_id' => $id])->all();

        $dataProvider= new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 1,
                ],
            ]);

        return $this->render('index', [
                'models' => $models,
                'pages' => $pages,
                'kategori' => $kategori,
                'dataProvider' => $dataProvider,
            ]);

    }
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new item();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionBuy($id){
        $cookies = Yii::$app->request->cookies;
        if($cookies->has('totalBeli')){
            $totalBeli = $cookies->get('totalBeli');
            $total = $totalBeli->value;
            $total++;

            $cookies=Yii::$app->response->cookies;

            $cookies->add(new Cookie([
                'name' => 'totalBeli',
                'value' => $total,
             ]));

            $cookies->add(new Cookie([
                 'name' => 'beli'. $total,
                'value' => $id,
             ]));

        }
        else{
            $cookies=Yii::$app->response->cookies;

            $cookies->add(new Cookie([
                'name' => 'totalBeli',
                'value' => 0,
             ]));

            $cookies->add(new Cookie([
                 'name' => 'beli'. 0,
                'value' => $id,
             ]));

        }
        return $this->redirect(['item/index']);


    }

    /**
     * Finds the item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = item::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
