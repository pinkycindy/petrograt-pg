<?php
    namespace frontend\components;
    use Yii;
    use Yii\base\component;
    use frontend\models\Statistic;

    class statisticRecord extends component
    {
        const EVEN_RECORD_STATISTIC = 'visit';
        public function statisticHandler(){
          $static=[
            'access_time'   =>  date('Y-m-d H:i:s'),
            'user_ip'       =>  Yii::$app->request->userIp,
            'user_host'     =>  Yii::$app->request->userHost,
            'path_info'     =>  Yii::$app->request->pathInfo,
            'query_string'  =>  Yii::$app->request->queryString,
          ];

          $model = new Statistic(); //manggil constructor Statistic
          $model->addStatistic($static);
        }

    }

?>
