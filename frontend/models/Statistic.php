<?php

namespace frontend\models;

use Yii;
//Pinky Cindy Arie Pradina
/**
 * This is the model class for table "statistic".
 *
 * @property integer $id
 * @property string $access_time
 * @property string $user_ip
 * @property string $user_host
 * @property string $path_info
 * @property string $query_string
 */
class Statistic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'statistic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['access_time'], 'safe'],
            [['user_ip'], 'string', 'max' => 20],
            [['user_host', 'path_info', 'query_string'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'access_time' => 'Access Time',
            'user_ip' => 'User Ip',
            'user_host' => 'User Host',
            'path_info' => 'Path Info',
            'query_string' => 'Query String',
        ];
    }

    public function addStatistic($static){
        if($static['user_host'] == NULL && $static['query_string'] == ""){
          $static['user_host']="-";
          $static['query_string']="-";
        };

        $this->access_time=$static['access_time'];
        $this->user_ip=$static['user_ip'];
        $this->user_host=$static['user_host'];
        $this->path_info=$static['path_info'];
        $this->query_string=$static['query_string'];

        $this->save();

    }
}
