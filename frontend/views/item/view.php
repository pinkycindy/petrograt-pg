<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\item */
//Pinky Cindy Arie Pradina
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
     <?php // Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php /*Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])*/ ?>
        <?= Html::a('Add to Chart', ['buy', 'id' => $model->id], ['class' => 'btn btn-success'])?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'price',
            'category_id',
            'created_at',
            'updated_at',
            'created_by',
            'updated_by',
            [
               'attributes' => 'image',
                'label' => 'Image',
                'value' => 'http://127.0.0.1/mymart/backend/web/images/' . $model->image,
                'format' =>['image', ['width'=>200, 'height'=>150]],
            ]
        ],
    ]) ?>

</div>
