<?php

use yii\helpers\Html;
use yii\grid\GridView;
//Pinky Cindy Arie Pradina
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\itemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-index">


    <h1><?= Html::encode($this->title) ?></h1>
    <select id="category">
        <option>Pilih Kategori</option>
        <option value="">semua</option>
        <?php foreach($kategori as $key){?>
        <option value="<?=$key['id']?>"><?=$key['name']?></option>
        <?php } ?>
    </select>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php // Html::a('Create Item', ['create'], ['class' => 'btn btn-success']) ?> 
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'price',

            ['class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'template' => '{view}',
            ],
        ],
    ]); ?>
</div>
<script type="text/javascript" src="<?=yii::$app->homeUrl?>js/jquery.min.js"></script>
<script type="text/javascript">
    $('#category').on('change', function () {
        var id = $('#category').val();
        if(id==''){
            location.href = '<?= yii::$app->homeUrl?>item';
        }else{
            location.href = '<?= yii::$app->homeUrl?>item/index-filter?id='+id;
        }
    });

</script>