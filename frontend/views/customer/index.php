<?php

use yii\helpers\Html;
use yii\grid\GridView;
//Pinky Cindy Arie Pradina
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\customerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php
            if ($customer == 0 )
            echo
            Html::a('Create Customer', ['create'], ['class' => 'btn btn-success']) 
        ?>

    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nama',
            'email:email',
            'user_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
