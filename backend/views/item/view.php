<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\item */
//Pinky Cindy Arie Pradina
$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
     <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'price',
            'category_id',
            [
                'attribute' => 'created_at',
                'format' => ['date']
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['date']
            ],
            'createdBy.username',
            'updatedBy.username',
            [
                'attribute' => 'image',
                'label' => 'Gambar',
                'value' => '../images/' . $model->image,
                'format' => ['image', ['width' => 200, 'height'=>150]],
            ],
        ],
    ]) ?>

</div>
