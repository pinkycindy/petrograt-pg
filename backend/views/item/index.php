<?php

use yii\helpers\Html;
use yii\grid\GridView;
//Pinky Cindy Arie Pradina
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\itemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Items';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="item-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Item', ['create'], ['class' => 'btn btn-success']) ?> 
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'price',
            'category_id',
            [
                'attribute' => 'created_at',
                'format' => ['date']
            ],
            [
                'attribute' => 'updated_at',
                'format' => ['date']
            ],
            'createdBy.username',
            'updatedBy.username',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
