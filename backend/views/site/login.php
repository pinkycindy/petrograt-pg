<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

 <div class="login-box" style="margin-top:70px; background-color: #FFF; border:1px solid #D2CEBA;">
    <div class="login-logo">
        <a href="#"> <img src="../images/logo.jpg" width="300px"></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body" style="background-color: #FFF;  padding-top: 0px" >
       <!-- <p class="login-box-msg"><h2 align="center">Petrokimia Gratifikasi!</h2></p> -->
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

            
                <div class="form-group" align="center">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-warning', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
