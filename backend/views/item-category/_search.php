<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//Pinky Cindy Arie Pradina
/* @var $this yii\web\View */
/* @var $model frontend\models\itemCategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="item-category-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'parent_category') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
