-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 26 Feb 2018 pada 06.29
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_petrograt`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `password` varchar(15) DEFAULT NULL,
  `nik` varchar(15) DEFAULT NULL,
  `jabatan` varchar(35) DEFAULT NULL,
  `unit_kerja` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faq`
--

CREATE TABLE `faq` (
  `id_faq` int(11) NOT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `pertanyaan` text,
  `jawaban` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `faq`
--

INSERT INTO `faq` (`id_faq`, `id_admin`, `pertanyaan`, `jawaban`) VALUES
(1, 1, 'Apa itu PETROGRAT ?', 'PETROGRAT adalah salah satu media online yang berisi sistem pelaporan gratifikasi PT. PETROKIMIA GRESIK. Setiap individu yang bekerja dan menerima upah dalam hubungan kerja dengan Perusahaan wajib menyampaikan laporan gratifikasi sesuai dengan peraturan yang berlaku.'),
(2, 1, 'Apa itu Gratifikasi ?', 'Gratifikasi yaitu pemberian dalam arti luas, yakni meliputi pemberian uang, barang, rabat (discount), komisi, pinjaman tanpa bunga, tiket perjalanan, fasilitas penginapan, perjalanan wisata, pengobatan cuma-cuma, dan fasilitas lainnya. Gratifikasi tersebut baik yang diterima secara langsung atau melalui keluarga inti, diterima di dalam negeri maupun di luar negeri dan yang dilakukan dengan menggunakan sarana elektronik atau tanpa sarana elektronik. '),
(3, 1, 'Apakah saya wajib mengisi nomor NIK ?', 'Nomor NIK wajib diisi ketika Anda akan melakukan login di aplikasi PETROGRAT ini.'),
(4, 1, 'Bagaimana cara saya membuat akun baru ?', 'Mohon maaf, aplikasi ini hanya di khususkan untuk individu yang bekerja atas nama Perusahaan PT. PETROKIMIA GRESIK. Apabila anda bekerja di PT. PETROKIMIA GRESIK, anda hanya perlu melakukan login dengan menggunakan nik dan password'),
(5, 1, 'Mengapa saya merasa aplikasi ini agak lambat ketika melakukan pelaporan hingga muncul pernyataan bahwa proses pelaporan gratifikasi saya sudah berhasil ?', 'Perangkat yang Anda gunakan harus memiliki minimal Android Operating System (OS) 4.4 Kitkat. Dan pastikan juga perangkat Anda terhubung dengan jaringan 3G/4G atau WiFi yang stabil'),
(6, 1, 'Apa itu kategori Penerimaan (Suap) dalam Gratifikasi ?', 'Gratifikasi dalam kategori ini merupakan penerimaan dalam bentuk apapun yang diperoleh insan Perusahaan dari pihak-pihak yang diduga memiliki keterkaitan dengan jabatan penerima. Gratifikasi tersebut haruslah merupakan penerimaan yang dilarang atau tidak sah secara hukum. Prinsip penerimaan tersebut harus DITOLAK, namun tidak dapat dilakukan penolakan dikarenakan akan berdampak buruk untuk perusahaan dan tidak diketahui proses pemberiannya/identitas dan alamat pemberi.'),
(7, 1, 'Apa itu kategori Penerimaan (Kedinasan) dalam Gratifikasi ?', 'Dalam acara resmi kedinasan atau penugasan yang dilaksanakan oleh insan Perusahaan, pemberian-pemberian seperti plakat, cinderamata, goody bag/gimmick, dan fasilitas pelatihan lainnya merupakan praktik yang dianggap wajar dan tidak berseberangan dengan standar etika yang berlaku. Gratifikasi yang diterima oleh insan Perusahaan tersebut ditujukan atau diperuntukkan kepada Perusahaan, bukan kepada personal yang mewakili Perusahaan. '),
(8, 1, 'Apa itu kategori Penolakan dalam Gratifikasi ?', 'Gratifikasi dalam kategori ini merupakan penolakan gratifikasi dalam bentuk apapun yang diperoleh insan Perusahaan dari pihak-pihak yang diduga memiliki keterkaitan dengan jabatan penerima. Penolakan yang dilakukan tersebut tidak berdampak buruk untuk perusahaan serta identitas dan alamat pemberi sangat jelas tertera.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lapor_kpk`
--

CREATE TABLE `lapor_kpk` (
  `id_lapor_kpk` int(11) NOT NULL,
  `id_penerimaan` int(11) NOT NULL,
  `kd_penerimaan` varchar(35) DEFAULT NULL,
  `uraian_barang` text,
  `kd_peristiwa` varchar(35) DEFAULT NULL,
  `jabatan_pemberi` varchar(35) DEFAULT NULL,
  `alamat_pemberi` text,
  `telp_pemberi` varchar(13) DEFAULT NULL,
  `email_pemberi` varchar(35) DEFAULT NULL,
  `faq_pemberi` varchar(13) DEFAULT NULL,
  `hubungan_dg_pemberi` varchar(40) DEFAULT NULL,
  `alasan_pemberian` text,
  `kronologi_pemberian` text,
  `tgl_lapor` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lapor_penerimaan`
--

CREATE TABLE `lapor_penerimaan` (
  `id_penerimaan` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `nama_barang` varchar(35) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `nilai_total_rp` int(11) DEFAULT NULL,
  `tgl_penerimaan` date DEFAULT NULL,
  `pihak_pemberi` varchar(50) DEFAULT NULL,
  `tempat_pemberian` varchar(50) DEFAULT NULL,
  `keterangan` text,
  `tgl_penyerahan_brg` date DEFAULT NULL,
  `jenis_penerimaan` varchar(30) DEFAULT NULL,
  `tgl_lapor` date DEFAULT NULL,
  `status_not` int(1) DEFAULT NULL,
  `status_barang` varchar(30) DEFAULT NULL,
  `lampiran` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lapor_penolakan`
--

CREATE TABLE `lapor_penolakan` (
  `id_penolakan` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `nama_barang` varchar(35) DEFAULT NULL,
  `nilai` int(11) DEFAULT NULL,
  `pemberi` varchar(35) DEFAULT NULL,
  `tempat` varchar(50) DEFAULT NULL,
  `tgl_penolakan` date DEFAULT NULL,
  `keterangan` text,
  `tgl_lapor` date DEFAULT NULL,
  `status_not` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1519180153),
('m130524_201442_init', 1519187388);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'SaEt7mltRja1TOKETcuZzCwxYL_0r48y', '$2y$10$30V8xezoGMsi5VLlzUUQCO8zS./S3RqZk6O481lvUfz6iIeBOD3QW', NULL, 'admin@gmail.com', 10, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_pelapor`
--

CREATE TABLE `user_pelapor` (
  `id_user` int(11) NOT NULL,
  `nik` int(15) NOT NULL,
  `password` varchar(200) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jabatan` varchar(35) NOT NULL,
  `unit_kerja` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL,
  `tempat_lahir` varchar(35) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user_pelapor`
--

INSERT INTO `user_pelapor` (`id_user`, `nik`, `password`, `nama`, `jabatan`, `unit_kerja`, `alamat`, `telp`, `email`, `tempat_lahir`, `tgl_lahir`, `foto`) VALUES
(1, 123, 'pinky', 'pinky', 'Staff Ahli', 'Dept. Tekinfo', '', '', '', '', '0000-00-00', ''),
(2, 222, 'tika', 'tika', 'Staff Ahli', 'Dept. Tekinfo', '', '', '', '', '0000-00-00', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id_faq`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indexes for table `lapor_kpk`
--
ALTER TABLE `lapor_kpk`
  ADD PRIMARY KEY (`id_lapor_kpk`),
  ADD KEY `id_penerimaan` (`id_penerimaan`);

--
-- Indexes for table `lapor_penerimaan`
--
ALTER TABLE `lapor_penerimaan`
  ADD PRIMARY KEY (`id_penerimaan`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `lapor_penolakan`
--
ALTER TABLE `lapor_penolakan`
  ADD PRIMARY KEY (`id_penolakan`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Indexes for table `user_pelapor`
--
ALTER TABLE `user_pelapor`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id_faq` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `lapor_kpk`
--
ALTER TABLE `lapor_kpk`
  MODIFY `id_lapor_kpk` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lapor_penerimaan`
--
ALTER TABLE `lapor_penerimaan`
  MODIFY `id_penerimaan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `lapor_penolakan`
--
ALTER TABLE `lapor_penolakan`
  MODIFY `id_penolakan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_pelapor`
--
ALTER TABLE `user_pelapor`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `lapor_kpk`
--
ALTER TABLE `lapor_kpk`
  ADD CONSTRAINT `lapor_kpk_ibfk_1` FOREIGN KEY (`id_penerimaan`) REFERENCES `lapor_penerimaan` (`id_penerimaan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `lapor_penerimaan`
--
ALTER TABLE `lapor_penerimaan`
  ADD CONSTRAINT `lapor_penerimaan_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user_pelapor` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `lapor_penolakan`
--
ALTER TABLE `lapor_penolakan`
  ADD CONSTRAINT `lapor_penolakan_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user_pelapor` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
