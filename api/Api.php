<?php 

	require_once 'DbConnect.php';
	
	//an array to display response
	$response = array();
	
	//if it is an api call 
	//that means a get parameter named api call is set in the URL 
	//and with this parameter we are concluding that it is an api call

	if(isset($_GET['apicall'])){
		
		switch($_GET['apicall']){
			
			case 'login':
				//for login we need the username and password 
				if(isTheseParametersAvailable(array('nik', 'password'))){
					//getting values 
					$nik = $_POST['nik'];
					$password = md5($_POST['password']); 
					
					//creating the query 
					$stmt = $conn->prepare("SELECT * FROM user WHERE nik = ? AND password = ? ");
					$stmt->bind_param("ss",$nik, $password);
					
					$stmt->execute();
					
					$stmt->store_result();
					
					//if the user exist with given credentials 
					if($stmt->num_rows > 0){
						
						$stmt->bind_result($id_user, $nik, $nama, $jabatan, $unit_kerja, $alamat, $telp, $email, $tempat_lahir, $tgl_lahir, $foto);
						$stmt->fetch();
						
						$user = array(
							'id_user'=>$id_user, 
							'nik'=>$nik, 
							'nama'=>$nama,
							'jabatan'=>$jabatan,
							'unit_kerja'=>$unit_kerja,
							'alamat'=>$alamat,
							'telp'=>$telp,
							'email'=>$email,
							'tempat_lahir'=>$tempat_lahir,
							'tgl_lahir'=>$tgl_lahir,
							'foto'=>$foto
						);
						
						$response['error'] = false; 
						$response['message'] = 'Login successfull'; 
						$response['user'] = $user; 
					}else{
						//if the user not found 
						$response['error'] = false; 
						$response['message'] = 'Invalid nik or password';
					}
				}
			break; 
			
			default: 
				$response['error'] = true; 
				$response['message'] = 'Invalid Operation Called';
		}
		
	}else{
		//if it is not api call 
		//pushing appropriate values to response array 
		$response['error'] = true; 
		$response['message'] = 'Invalid API Call';
	}
	
	//displaying the response in json structure 
	echo json_encode($response);
	
	//function validating all the paramters are available
	//we will pass the required parameters to this function 
	function isTheseParametersAvailable($params){
		
		//traversing through all the parameters 
		foreach($params as $param){
			//if the paramter is not available
			if(!isset($_POST[$param])){
				//return false 
				return false; 
			}
		}
		//return true if every param is available 
		return true; 
	}