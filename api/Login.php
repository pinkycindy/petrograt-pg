<?php
    require("password.php");

    $con = mysqli_connect("localhost", "root", "", "db_petrograt");

    $nik = $_POST['nik'];
    $password = $_POST['password'];

    $statement = mysqli_prepare($con, "SELECT * FROM user_pelapor WHERE nik = ?");
    mysqli_stmt_bind_param($statement, "s", $nik);
    mysqli_stmt_execute($statement);
    mysqli_stmt_store_result($statement);
    mysqli_stmt_bind_result($statement, $colIdUser, $colNik, $colPassword, $colNama, $colJabatan, $colUnitKerja, $colAlamat, $colTelp, $colEmail, $colTempatLahir, $colTglLahir, $colFoto);

    $response = array();
    $response["success"] = false;

    while(mysqli_stmt_fetch($statement)){
        if ($password == $colPassword) {
          $response["success"] = true;
          $response["id_user"] = $colIdUser;
          $response["nik"] = $colNik;
          $response["password"] = $colPassword;
          $response["nama"] = $colNama;
          $response["jabatan"] = $colJabatan;
          $response["unit_kerja"] = $colUnitKerja;
          $response["alamat"] = $colAlamat;
          $response["telp"] = $colTelp;
          $response["email"] = $colEmail;
          $response["tempat_lahir"] = $colTempatLahir;
          $response["tgl_lahir"] = $colTglLahir;
          $response["foto"] = $colFoto;
        }
    }

    echo json_encode($response);

?>
